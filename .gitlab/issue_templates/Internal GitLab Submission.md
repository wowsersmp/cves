### Vulnerability Submission

#### Publishing Schedule

After a CVE request is validated, a CVE identifier will be assigned. On what
schedule should the details of the CVE be published? This may be updated at any time and signal when an advisory is ready to publish.

* [ ] Publish immediately
* [x] Wait to publish

#### Request CVE ID 
On initial submission, you may automatically request a CVE ID. Be aware, this will not work after the initial submission.

* [x] Automatically assign CVE ID
<!--
Please fill out the yaml codeblock below
-->

```yaml
reporter:
  name: "GitLab Security Team" # "First Last"
  email: "security@gitlab.com" # "email@domain.tld"
vulnerability:
  # If the affected product is GitLab, please mention if it's GitLab CE/EE or GitLab EE
  description: "TODO" # "[VULNTYPE] in [COMPONENT] in [VENDOR][PRODUCT] affecting all versions from 1X.X prior to 16.X.X, 16.Y prior to 16.Y.Y, and 16.Z prior to 16.Z.Z allows [ATTACKER] to [IMPACT] via [VECTOR]"
  cwe: "CWE-0" # TODO, for example "CWE-22" # Path Traversal
  product:
    gitlab_path: "gitlab-org/gitlab" # "namespace/project" # the path of the project within gitlab
    vendor: "GitLab" # "iTerm2"
    name: "GitLab" # "iTerm2"
    affected_versions:
      - ">=1X.X, <16.X.X" # "1.2.3"
      - ">=16.Y, <16.Y.Y" # "1.2.3"
      - ">=16.Z, <16.Z.Z" # "1.2.3"
    fixed_versions: [] # If empty, fixed versions will be automatically filled based on affected version constraints
    # fixed_versions:
    #  - "16.X.X"
    #  - "16.Y.Y"
    #  - "16.Z.Z"
  impact: "AV:N/AC:H/PR:L/UI:N/S:U/C:N/I:N/A:N" # CVSS:3.1 string, without the prefix
  solution: "" # If empty, solution will be automatically filled based on affected version constraints
  # solution: "Upgrade to version 16.X.X, 16.Y.Y or 16.Z.Z"
  credit: "Thanks [<ENTER REPORTER NAME>](https://hackerone.com/<ENTER REPORTER NAME>) for reporting this vulnerability through our HackerOne bug bounty program"
  # credit: "This vulnerability has been discovered internally by GitLab team member <TEAM MEMBER NAME>"
  references:
    - "https://gitlab.com/gitlab-org/gitlab/-/issues/[ENTER ISSUE]"
    - "https://hackerone.com/reports/[ENTER REPORT IF APPLICABLE]"
```


CVSS scores can be computed by means of the [GitLab CVSS Calculator](https://gitlab-com.gitlab.io/gl-security/appsec/cvss-calculator/) (see also the [CVSS Calculation Guide](https://about.gitlab.com/handbook/engineering/security/security-engineering-and-research/application-security/runbooks/cvss-calculation.html)).

Alternatively, you can use the [NVD CVSS Calculator](https://nvd.nist.gov/vuln-metrics/cvss/v3-calculator).

/label ~"devops::secure" ~"group::vulnerability research" ~"vulnerability research::cve" ~"advisory::queued" ~"advisory-group::gitlab"
/confidential
