# GitLab CVE assignments

This project stores a copy of all [Common Vulnerability Enumeration](#what-is-a-cve) (CVE) identifiers assigned and published by GitLab in its role as a [CVE Numbering Authority](#what-is-a-cna) (CNA).

<br>
<br>
<div align="center">
<a href="https://gitlab.com/gitlab-org/cves/-/issues/new?issue[confidential]=true&issuable_template=CVE%20Request&issue[title]=CVE%20Request%20For%20[project]"><img alt="Request a CVE Identifier" src="https://img.shields.io/badge/Request%20a%20CVE%20Identifier-white?style=for-the-badge&logo=gitlab&label=CNA&labelColor=171321&color=171321" />
</a><br>
<em>If you are a maintainer of a public project hosted on GitLab.com</em>
</div>

## What is a CVE?

Common Vulnerability Enumeration identifiers, or CVEs for short, are unique names given to specific vulnerabilities found in software, or systems. They take the form of `CVE-YYYY-NNNNN`, where `YYYY` is the year when the CVE is assigned, or when it's first shared publicly, and `NNNNN` is a unique number for that year. This method helps keep track of vulnerabilities in an orderly way, which makes it easier to study them, and fix the problems.

*Read more on the [CVE program About page](https://www.cve.org/About/Overview).*

## What is a CNA?

[CVE Numbering Authorities](https://www.cve.org/ProgramOrganization/CNAs), or CNAs for short, are responsible for assigning <abbr title="Common Vulnerability Enumeration">CVE</abbr> identifiers to vulnerabilities in software or systems they oversee. GitLab is participating in [MITRE's CNA program](https://www.cve.org/PartnerInformation/ListofPartners/partner/GitLab) and can assign CVE identifiers for any public projects hosted on GitLab.com.

*Read more about [GitLabs role as a CNA](https://about.gitlab.com/security/cve/).*

## Requesting a CVE for Your Project

If you're maintaining a *public* project on GitLab.com and discover a security issue, you can request a unique <abbr title="Common Vulnerability Enumeration">CVE</abbr> identifier for it. [Create a confidential issue](https://gitlab.com/gitlab-org/cves/-/issues/new?issue[confidential]=true&issuable_template=CVE%20Request&issue[title]=CVE%20Request%20For%20[project]) in this project and remember to use the `CVE Request` issue template. This ensures we get all the necessary details for the assignment.

After the issue is created, an automated system handles the submission through different stages, from validation to CVE assignment, all the way to final publishing with [MITRE](https://cve.mitre.org/).

A successful CVE request goes through the following stages:

|           ~advisory::queued →           |         ~advisory::reviewing →        |                 ~advisory::assign-cve →                 |                   ~advisory::assigned →                   |                  ~advisory::publishing →                  |             ~advisory::published             |
|:---------------------------------------:|:-------------------------------------:|:-------------------------------------------------------:|:---------------------------------------------------------:|:---------------------------------------------------------:|:--------------------------------------------:|
| Advisory is new and awaiting validation | Advisory is valid and awaiting review | Advisory is reviewed and awaiting CVE number assignment | Advisory is assigned a CVE ID and awaiting final approval | Advisory is submitted to MITRE and awaiting status change | Advisory is published and available on MITRE |

The system lets you know via issue comments about events on your CVE request throughout the process.
